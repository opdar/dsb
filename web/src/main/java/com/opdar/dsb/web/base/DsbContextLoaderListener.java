package com.opdar.dsb.web.base;

import com.opdar.dsb.web.entities.InitializeEntity;
import com.opdar.dsb.web.entities.UsersEntity;
import com.opdar.dsb.web.session.ISessionManager;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import java.util.Properties;

/**
 * Created by jeffrey on 2016/11/12.
 */
public class DsbContextLoaderListener extends ContextLoaderListener {
    @Override
    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);
        WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
        Properties properties = GlobalUtil.getGlobalProperties();
        boolean initialze = Boolean.valueOf(properties.getProperty("initialize","false"));
        if(initialze){
            //已经初始化过，初始化数据源
            String host = properties.getProperty("host","127.0.0.1");
            String port = properties.getProperty("port","3306");
            String databaseName = properties.getProperty("databaseName","dsbcenter");
            String userName = properties.getProperty("userName","root");
            String passWord = properties.getProperty("passWord","");
            String prefix = properties.getProperty("prefix","t_");
            InitializeEntity initializeEntity = new InitializeEntity(host,port,databaseName,userName,passWord,prefix);

            DefaultListableBeanFactory acf = (DefaultListableBeanFactory) applicationContext
                    .getAutowireCapableBeanFactory();

            acf.registerSingleton("initializeEntity",initializeEntity);
            GlobalUtil.initDataSource(applicationContext,initializeEntity);
            //初始化Mybatis
            GlobalUtil.initSessionFactory(applicationContext);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
        //优雅关闭Session池
        ISessionManager<UsersEntity> iSessionManager = applicationContext.getBean(ISessionManager.class);
        iSessionManager.destory();
        //销毁
        super.contextDestroyed(event);
    }
}
