package com.opdar.dsb.web.base;

import com.opdar.dsb.web.entities.InitializeEntity;
import com.opdar.dsb.web.mybatis.DsbSqlSessionFactoryBean;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import sun.util.locale.BaseLocale;

import javax.sql.DataSource;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 俊帆 on 2016/11/11.
 */
public class GlobalUtil {
    public static final String GLOBAL_PROPERTIES = "global.properties";
    private static Properties properties;
    private static Map<Integer,String> tips = new HashMap<Integer, String>();
    {
        try {
            ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource[] resources=resolver.getResources("classpath*:com/opdar/dsb/message/lang.*");
            for(Resource resource:resources){
                System.out.println(resource.getFilename());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ResourceBundle bundle = ResourceBundle.getBundle("com/opdar/dsb/message/lang",Locale.ENGLISH);
        System.out.println(bundle.getKeys().nextElement());
    }
    public static void getTipsMessage(){

    }

    public static Properties getGlobalProperties() {
        if (properties != null) return properties;
        properties = new Properties();
        FileInputStream fileInputStream = null;
        try {

            fileInputStream = new FileInputStream(new File(Thread.currentThread().getContextClassLoader().getResource("/").getPath(),GLOBAL_PROPERTIES));
            properties.load(fileInputStream);
        } catch (Exception e) {
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return properties;
    }

    public static void initDataSource(ApplicationContext ctx, InitializeEntity initializeEntity) {
        DefaultListableBeanFactory acf = (DefaultListableBeanFactory) ctx
                .getAutowireCapableBeanFactory();
        if(acf.containsBean("dataSource")){
            try{
                HikariDataSource dataSource = ctx.getBean(HikariDataSource.class);
                dataSource.close();
            }catch (Exception ignored){}
            acf.removeBeanDefinition("dataSource");
        }
        BeanDefinitionBuilder dataSourceBuider = BeanDefinitionBuilder.genericBeanDefinition(HikariDataSource.class);
        HikariConfig config = new HikariConfig();
        config.setMaximumPoolSize(100);
        config.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        config.addDataSourceProperty("serverName", initializeEntity.getHost());
        config.addDataSourceProperty("port", initializeEntity.getPort());
        config.addDataSourceProperty("user", initializeEntity.getUserName());
        config.addDataSourceProperty("password", initializeEntity.getPassWord());

        dataSourceBuider.addConstructorArgValue(config);
        acf.registerBeanDefinition("dataSource", dataSourceBuider.getBeanDefinition());
    }

    public static void initSessionFactory(ApplicationContext ctx){
        if(ctx.containsBean("sessionFactory"))return;
        HikariDataSource dataSource = ctx.getBean(HikariDataSource.class);
        try {
            ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            //只加载一个绝对匹配Resource,且通过ResourceLoader.getResource进行加载
            Resource[] resources=resolver.getResources("classpath*:com/opdar/dsb/mapper/*.xml");
            BeanDefinitionBuilder sqlSessionFactoryBuilder = BeanDefinitionBuilder.genericBeanDefinition(DsbSqlSessionFactoryBean.class)
                    .addConstructorArgValue(dataSource)
                    .addConstructorArgValue(resources);
            DefaultListableBeanFactory acf = (DefaultListableBeanFactory) ctx
                    .getAutowireCapableBeanFactory();
            if(acf.containsBean("sessionFactory")){
                acf.removeBeanDefinition("sessionFactory");
            }
            acf.registerBeanDefinition("sessionFactory", sqlSessionFactoryBuilder.getBeanDefinition());

            MapperScannerConfigurer configurer = new MapperScannerConfigurer();
            configurer.setBasePackage("com.opdar.dsb.web.mapper");
            BeanDefinitionRegistry registry = (BeanDefinitionRegistry) ctx.getAutowireCapableBeanFactory();
            configurer.postProcessBeanDefinitionRegistry(registry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void putGlobalProperties(Map<String, String> map) {
        for(Map.Entry<String, String> entry:map.entrySet()){
            getGlobalProperties().put(entry.getKey(),entry.getValue());
        }
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(new File(Thread.currentThread().getContextClassLoader().getResource("/").getPath(),GLOBAL_PROPERTIES));
            getGlobalProperties().store(fileOutputStream, String.valueOf(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date())));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void createBaseTables(DataSource dataSource, Resource tables) {
        Connection connection = null;
        Statement stat = null;
        try {
            connection = dataSource.getConnection();
            stat = connection.createStatement();

            String dbName = getGlobalProperties().getProperty("databaseName", "dsbcenter");
            String prefix = getGlobalProperties().getProperty("prefix", "t_");
            stat.execute("CREATE DATABASE IF NOT EXISTS " + dbName + " DEFAULT CHARSET utf8 COLLATE utf8_general_ci; ");
            stat.execute("use " + dbName + ";");
            stat.close();

            //create tables

            BufferedReader reader = new BufferedReader(new InputStreamReader(tables.getInputStream()));
            String line = null;
            LinkedList<String> sqls = new LinkedList<String>();
            while ((line = reader.readLine()) != null) {
                sqls.add(line.replace("#{prefix}", prefix));
            }
            stat = connection.createStatement();
            stat.execute("start transaction" + ";");
            for(String sql:sqls){
                stat.execute(sql);
            }
            stat.execute("COMMIT;");
        } catch (Exception e) {
            e.printStackTrace();
            try {
                if (stat != null) {
                    stat.execute("ROLLBACK;");
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                if (stat != null) {
                    stat.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
