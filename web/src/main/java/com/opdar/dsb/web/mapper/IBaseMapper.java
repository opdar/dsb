package com.opdar.dsb.web.mapper;

import java.util.List;

/**
 * Created by jeffrey on 2016/11/12.
 */
public interface IBaseMapper<T> {
    public int insert(T o);
    public int update(T o);
    public T selectOne(T o);
    public List<T> selectList(T o);
}
