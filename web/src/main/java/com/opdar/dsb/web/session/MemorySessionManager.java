package com.opdar.dsb.web.session;

import com.opdar.dsb.web.entities.UsersEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by jeffrey on 2016/11/13.
 */
public class MemorySessionManager implements ISessionManager<UsersEntity> {
    private static ConcurrentHashMap<String,SessionTask> sessions = new ConcurrentHashMap<String, SessionTask>();
    private static ExecutorService executorService = Executors.newCachedThreadPool();
    private Logger logger = LoggerFactory.getLogger("SessionManager");
    private AtomicBoolean isDestory = new AtomicBoolean(false);

    public void set(String token,UsersEntity users,long timeout){
        SessionTask task = null;
        sessions.put(token,task = new SessionTask(token,users,timeout));
        executorService.execute(task);
    }

    @Override
    public UsersEntity get(String token) {
        return sessions.containsKey(token)?sessions.get(token).getUsersEntity():null;
    }

    @Override
    public void clearTimeout(String token) {
        sessions.get(token).clearTimeout();
    }

    @Override
    public void destory() {
        executorService.shutdown();
    }

    public void remove(String token){
        sessions.remove(token);
    }

    private class SessionTask implements Runnable {
        private String token;
        private UsersEntity usersEntity;
        private long timeout = 60*1000;
        private long lastRefreshTime = System.currentTimeMillis();
        private AtomicBoolean isRunning = new AtomicBoolean(true);

        UsersEntity getUsersEntity() {
            return usersEntity;
        }

        SessionTask(String token, UsersEntity usersEntity, long timeout) {
            this.token = token;
            this.usersEntity = usersEntity;
            this.timeout = timeout;
        }

        void clearTimeout(){
            lastRefreshTime = System.currentTimeMillis();
        }

        @Override
        public void run() {
            while (isRunning.get() && !MemorySessionManager.this.isDestory.get()){
                long t = System.currentTimeMillis() - lastRefreshTime;
                if(t >= timeout){
                    //超时
                    isRunning.set(false);
                    logger.debug("用户「{}」登陆超时",token);
                    MemorySessionManager.this.remove(token);
                }else{
                    long nextScheduleTime = timeout - t;
                    try {
                        Thread.sleep(nextScheduleTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
