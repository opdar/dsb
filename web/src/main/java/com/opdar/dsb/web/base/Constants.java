package com.opdar.dsb.web.base;

/**
 * Created by jeffrey on 2016/11/12.
 */
public class Constants {
    public static final String CLASSPATH_TABLES_SQL = "classpath:tables.sql";
    public static final int SESSION_TIMEOUT = 300*1000;//5分钟
    public static class Msg{
        public static final int SUCCESS = 0;
        public static final int UNKNOWN = 9999;
        public static final int USER_NOT_REGIST = 1;
        public static final int PASSWORD_INCORRECT = 2;
    }

}
