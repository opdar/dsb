package com.opdar.dsb.web.controllers;

import com.opdar.dsb.web.base.Constants;
import com.opdar.dsb.web.base.GlobalUtil;
import com.opdar.dsb.web.entities.InitializeEntity;
import com.opdar.dsb.web.entities.UsersEntity;
import com.opdar.dsb.web.mapper.UsersMapper;
import com.opdar.dsb.web.utils.SHA1;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

/**
 * 初始化Page
 * Created by 俊帆 on 2016/11/11.
 */
@Controller
@RequestMapping("/init")
public class InitializeController {
    @Autowired
    private ApplicationContext ctx;

    @RequestMapping("step1.html")
    public String index() {
        Properties properties = GlobalUtil.getGlobalProperties();
        boolean initialze = Boolean.valueOf(properties.getProperty("initialize","false"));
        if(initialze){
            return "init/error";
        }else{
            return "init/step1";
        }
    }

    @RequestMapping("step2.html")
    public String step2(InitializeEntity initializeEntity,  Model model) {
        Properties properties = GlobalUtil.getGlobalProperties();
        boolean initialze = Boolean.valueOf(properties.getProperty("initialize","false"));
        if(initialze){
            return "init/error";
        }else{
            try{
                //初始化数据源
                GlobalUtil.initDataSource(ctx,initializeEntity);
                //存储数据源
                GlobalUtil.putGlobalProperties(initializeEntity.getMap());
                //创建表
                GlobalUtil.createBaseTables(ctx.getBean(HikariDataSource.class),ctx.getResource(Constants.CLASSPATH_TABLES_SQL));

                return "init/step2";
            }catch (Exception e){
                model.addAttribute("error",e);
                return "init/step1";
            }
        }
    }

    @RequestMapping("complate.html")
    public String complate(UsersEntity usersEntity, Model model) {
        Properties properties = GlobalUtil.getGlobalProperties();
        boolean initialze = Boolean.valueOf(properties.getProperty("initialize","false"));
        if(initialze){
            return "init/error";
        }else{
            GlobalUtil.initSessionFactory(ctx);
            UsersMapper usersMapper = ctx.getBean(UsersMapper.class);
            usersEntity.setSalt(UUID.randomUUID().toString());
            usersEntity.setUserPwd(SHA1.encrypt(usersEntity.getUserPwd()+usersEntity.getSalt()));
            usersEntity.setId(UUID.randomUUID().toString());
            usersMapper.insert(usersEntity);
            //初始化成功
            Map<String, String> map = new HashMap<String, String>();
            map.put("initialize","true");
            GlobalUtil.putGlobalProperties(map);
            return "init/complate";
        }
    }
}
