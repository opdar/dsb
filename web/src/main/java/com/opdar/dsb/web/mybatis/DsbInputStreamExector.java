package com.opdar.dsb.web.mybatis;

import com.opdar.dsb.web.base.GlobalUtil;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;

import java.io.*;

/**
 * Created by jeffrey on 2016/11/13.
 */
public class DsbInputStreamExector {

    public static Resource encode(Resource mapperLocation){
        try {
            String tablePrefix = GlobalUtil.getGlobalProperties().getProperty("prefix","t_");
            String databaseName = GlobalUtil.getGlobalProperties().getProperty("databaseName","dsbcenter");
            BufferedReader reader = new BufferedReader(new InputStreamReader(mapperLocation.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null){
                stringBuilder.append(line.replaceAll("`dsbcenter`.`t_","`"+databaseName+"`.`"+tablePrefix+""));
            }
            return new InputStreamResource(new ByteArrayInputStream(stringBuilder.toString().getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
