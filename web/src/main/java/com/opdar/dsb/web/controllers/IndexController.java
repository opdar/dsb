package com.opdar.dsb.web.controllers;

import com.opdar.dsb.web.base.Constants;
import com.opdar.dsb.web.base.GlobalUtil;
import com.opdar.dsb.web.entities.UsersEntity;
import com.opdar.dsb.web.mapper.UsersMapper;
import com.opdar.dsb.web.session.ISessionManager;
import com.opdar.dsb.web.utils.SHA1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Properties;
import java.util.UUID;

/**
 * Created by 俊帆 on 2016/11/11.
 */
@Controller
@RequestMapping("/")
public class IndexController {
    @Autowired
    ISessionManager<UsersEntity> sessionManager;
    @Autowired
    ApplicationContext ctx;

    @RequestMapping("/")
    public String index(){
        return "redirect:login.html";
    }

    @RequestMapping("/login.html")
    public String login(UsersEntity usersEntity,HttpServletRequest request){
        if(usersEntity.getUserName() != null){
            UsersEntity query = new UsersEntity();
            query.setUserName(usersEntity.getUserName());
            UsersEntity u = ctx.getBean(UsersMapper.class).selectOne(query);
            if(u == null){
                //该用户不存在
                return "login/index";
            }
            String password = SHA1.encrypt(usersEntity.getUserPwd()+u.getSalt());
            if(u.getUserPwd().equals(password)){
                String token = UUID.randomUUID().toString();
                request.getSession().setAttribute("token",token);
                u.setUserPwd(null);
                sessionManager.set(token,u, Constants.SESSION_TIMEOUT);
                return "redirect:/dashboard/index.html";
            }else{
                //密码有误，请核实后重新输入
                return "login/index";
            }
        }
        Properties properties = GlobalUtil.getGlobalProperties();
        boolean initialze = Boolean.valueOf(properties.getProperty("initialize","false"));
        if(!initialze){
            return "redirect:init/step1.html";
        }else{
            return "login/index";
        }
    }
}
