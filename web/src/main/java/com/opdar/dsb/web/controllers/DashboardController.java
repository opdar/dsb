package com.opdar.dsb.web.controllers;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Created by jeffrey on 2016/11/12.
 */
@Controller
@Lazy
@RequestMapping("/dashboard")
public class DashboardController {

    @RequestMapping("/index.html")
    public String index(){
        return "dashboard/index";
    }
}
