package com.opdar.dsb.web.entities;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 俊帆 on 2016/11/11.
 */
public class InitializeEntity {
    private String host;
    private String port;
    private String databaseName;
    private String userName;
    private String passWord;
    private String prefix;

    public InitializeEntity() {
    }

    public InitializeEntity(String host, String port, String databaseName, String userName, String passWord, String prefix) {
        this.host = host;
        this.port = port;
        this.databaseName = databaseName;
        this.userName = userName;
        this.passWord = passWord;
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Map<String,String> getMap(){
        Map<String,String> map = new HashMap<String, String>();
        map.put("host",host);
        map.put("port",port);
        map.put("databaseName",databaseName);
        map.put("userName",userName);
        map.put("passWord",passWord);
        map.put("prefix",prefix);
        return map;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InitializeEntity{");
        sb.append("host='").append(host).append('\'');
        sb.append(", port='").append(port).append('\'');
        sb.append(", databaseName='").append(databaseName).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", passWord='").append(passWord).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
