package com.opdar.dsb.web.mapper;

import com.opdar.dsb.web.entities.UsersEntity;

/**
 * Created by jeffrey on 2016/11/12.
 */
public interface UsersMapper extends IBaseMapper<UsersEntity> {
}
