package com.opdar.dsb.web.base;

import com.opdar.dsb.web.utils.ReflectUtil;
import org.apache.ibatis.executor.statement.RoutingStatementHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.*;
import org.mybatis.spring.SqlSessionFactoryBean;

import java.sql.Connection;
import java.util.Properties;


/**
 * Created by jeffrey on 2016/11/12.
*/
@Intercepts({@Signature(
        type= StatementHandler.class,
        method = "prepare",
        args = {Connection.class,Integer.class})})
public class SchemaInterceptor implements Interceptor {
    String databaseName,tablePrefix;
    public Object intercept(Invocation invocation) throws Throwable {
        RoutingStatementHandler handler = (RoutingStatementHandler) invocation.getTarget();
        StatementHandler delegate = (StatementHandler) ReflectUtil.getFieldValue(handler, "delegate");
        BoundSql boundSql = delegate.getBoundSql();
        String sql = boundSql.getSql().replaceFirst("`dsbcenter`.`t_","`"+databaseName+"`.`"+tablePrefix);
        ReflectUtil.setFieldValue(boundSql, "sql", sql);
        return invocation.proceed();
    }

    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    public void setProperties(Properties properties) {
        databaseName = properties.getProperty("databaseName");
        tablePrefix = properties.getProperty("tablePrefix");
    }
}
