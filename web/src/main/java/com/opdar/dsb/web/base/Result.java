package com.opdar.dsb.web.base;

import java.util.Locale;

/**
 * Created by jeffrey on 2016/11/13.
 */
public class Result<T> {
    private Locale locale = Locale.CHINESE;
    private int code;
    private String msg;
    private T result;

    public Result(int code) {
        this.code = code;
    }

    public Result(int code, String msg, T result) {
        this.code = code;
        this.msg = msg;
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
