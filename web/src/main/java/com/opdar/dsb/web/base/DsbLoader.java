package com.opdar.dsb.web.base;

import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by jeffrey on 2016/11/13.
 */
public class DsbLoader extends URLClassLoader {
    public DsbLoader() {
        super(new URL[] {}, Thread.currentThread().getContextClassLoader());
    }
}
