<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <#include "/resource.ftl"/>
    <title>正在准备 Step-1</title>
</head>

<body style="background-color:#fffdfa;">

<div>
    <div style="font-size: 56px;text-align: center;margin-top: 2%;font-weight: bold;">正在准备</div>
    <div style="font-size: 16px;text-align: center;">Step-1</div>
    <form id="init-database" action="/init/step2.html" method="post"
          style="width: 560px;margin: 60px auto;padding: 14px;background-color: white;border: 1px solid #D0D0D0;border-radius: 8px;">
        <h3>数据库设置</h3>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-desktop" aria-hidden="true"></i>&nbsp;</span>
            <div class="form-group label-static is-empty">
                <input v-model="host" type="text" name="host" class="form-control" v-bind="{value:host}"
                       placeholder="请输入数据库地址">
                <p class="help-block">请输入数据库地址</p>
            </div>
            <span class="input-group-addon">:</span>
            <div class="form-group label-static is-empty">

                <input v-model="port" type="text" name="port" class="form-control" v-bind="{value:port}"
                       placeholder="请输入数据库端口">
                <p class="help-block">请输入数据库端口</p>
            </div>
        </div>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-database" aria-hidden="true"></i>&nbsp;</span>
            <div class="form-group label-static is-empty">
                <input v-model="databaseName" name="databaseName" type="text" class="form-control"
                       v-bind="{value:databaseName}" placeholder="请设置数据库名"/>
                <p class="help-block">请设置数据库名</code></p>
            </div>
        </div>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;</span>
            <div class="form-group label-static is-empty">
                <input v-model="tablePrefix" name="prefix" type="text" class="form-control"
                       v-bind="{value:tablePrefix}" placeholder="请设置数据表前缀"/>
                <p class="help-block">请设置数据表前缀，例：{{tablePrefix}}users</code></p>
            </div>
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;</span>
            <div class="form-group label-static is-empty">
                <input v-model="username" type="text" name="userName" class="form-control" v-bind="{value:username}"
                       placeholder="请输入数据库账号">
                <p class="help-block">请输入数据库账号</code></p>
            </div>
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
            <div class="form-group label-static is-empty">
                <input v-model="password" type="password" name="passWord" class="form-control" v-bind="{value:password}"
                       placeholder="请输入数据库密码">
                <p class="help-block">请输入数据库密码</code></p>
            </div>
        </div>

        <button type="button" @click="initialize" v-bind="{disabled:((field.length != 6)||(canSubmit))}"
                style="background-color: #8ed5d6;color: #fffdfa;margin-top: 24px;"
                class="btn btn-block btn-sup btn-material-pink btn-raised">下一步
        </button>
        <div style="margin-top: 32px;" v-if="initializing">
            <h4 style="font-weight:bold;text-align: center;">正在初始化...</h4>
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
        <div v-if="error.length>0" class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 16px;">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
                    class="sr-only">Close</span></button>
            {{error}}
        </div>
    </form>

    <script>
        var vm = new Vue({
            el: '#init-database',
            data: {
                host: '127.0.0.1',
                port: '3306',
                username: 'root',
                password: '',
                error: "${error}",
                databaseName: 'dsbcenter',
                field: [1, 2, 3, 5,6],
                passShow: [],
                tablePrefix:'t_',
                canSubmit: false,
                initializing: false
            },
            methods: {
                initialize: function (e) {
                    this.canSubmit = true;
                    this.initializing = true;
                    $('#init-database').submit();
                },
                clear: function (e) {
                    if (e == 'host') {
                        this.phone = '';
                    } else if (e == 'port') {
                        this.port = '';
                    } else if (e == 'username') {
                        this.username = '';
                    } else if (e == 'password') {
                        this.password = '';
                    } else if (e == 'databaseName') {
                        this.password = '';
                    }
                }
                ,
                passToggle: function (key) {
                    if (this.passShow.indexOf(key) == -1) {
                        this.passShow.push(key);
                    } else {
                        this.passShow.remove(key);
                    }
                },
                passStatus: function (key) {
                    return this.passShow.indexOf(key) != -1;
                }
            },
            watch: {
                host: function (host) {
                    counter(this.field, 1, host);
                },
                port: function (port) {
                    counter(this.field, 2, port);
                },
                username: function (username) {
                    counter(this.field, 3, username);
                },
                password: function (password) {
                    counter(this.field, 4, password);
                },
                databaseName: function (databaseName) {
                    counter(this.field, 5, databaseName);
                },
                tablePrefix: function (tablePrefix) {
                    counter(this.field, 6, tablePrefix);
                }
            }
        });
    </script>

    <#include "/footer.ftl"/>
</body>
</html>
