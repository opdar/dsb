<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<#include "/resource.ftl"/>
    <title>dsb-proxy | 成功啦！</title>
</head>

<body style="background-color:#fffdfa;">

<div>
    <div style="font-size: 56px;text-align: center;margin-top: 2%;font-weight: bold;">什么？竟然成功初始化了？</div>

    <button type="button" @click="initialize"
            style="background-color: #55b819;color: #fffdfa;margin: 24px auto;width: 360px;"
            class="btn btn-block btn-sup btn-material-pink btn-raised" onclick="window.location.href='/login.html'">去登陆
    </button>
<#include "/footer.ftl"/>
</body>
</html>
