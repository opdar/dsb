<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<#include "/resource.ftl"/>
    <title>登陆</title>
</head>

<body style="background-image: url('/imgs/bj.jpg')">


<div>
    <form id="init-users" method="post"
          style="width: 400px;margin: 60px auto;padding: 14px;background-color: white;border: 1px solid #D0D0D0;border-radius: 8px;">

        <div style="font-size: 28px;text-align: center;font-weight: bold;">登陆</div>
        <div style="font-size: 16px;text-align: center;">Login</div>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;</span>
            <div class="form-group label-static is-empty">
                <input v-model="username" name="userName" type="text" class="form-control"
                       v-bind="{value:username}" placeholder="请输入您的用户名"/>
                <p class="help-block">用户名由字母或数字组成</code></p>
            </div>
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
            <div class="form-group label-static is-empty">
                <input v-model="password" type="password" name="userPwd" class="form-control" v-bind="{value:password}"
                       placeholder="请设置一个密码">
                <p class="help-block">填什么就不用我说了吧？</p>
            </div>
        </div>

        <button type="button" @click="initialize" v-bind="{disabled:((field.length != 2)||(canSubmit))}"
                style="background-color: #8ed5d6;color: #fffdfa;margin-top: 24px;"
                class="btn btn-block btn-sup btn-material-pink btn-raised">完成
        </button>
        <div style="margin-top: 32px;" v-if="initializing">
            <h4 style="font-weight:bold;text-align: center;">正在登陆...</h4>
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
        <div v-if="error.length>0" class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 16px;">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
                    class="sr-only">Close</span></button>
            {{error}}
        </div>
    </form>

    <script>
        var vm = new Vue({
            el: '#init-users',
            data: {
                username: 'admin',
                password: 'admin',
                error: "${error}",
                field: [1, 2],
                passShow: [],
                canSubmit: false,
                initializing: false
            },
            methods: {
                initialize: function (e) {
                    this.canSubmit = true;
                    this.initializing = true;
                    $('#init-users').submit();
                },
                clear: function (e) {
                    if (e == 'username') {
                        this.username = '';
                    } else if (e == 'password') {
                        this.password = '';
                    }
                }
                ,
                passToggle: function (key) {
                    if (this.passShow.indexOf(key) == -1) {
                        this.passShow.push(key);
                    } else {
                        this.passShow.remove(key);
                    }
                },
                passStatus: function (key) {
                    return this.passShow.indexOf(key) != -1;
                }
            },
            watch: {
                username: function (username) {
                    counter(this.field, 1, username);
                },
                password: function (password) {
                    counter(this.field, 2, password);
                }
            }
        });
    </script>

<#include "/footer.ftl"/>
</body>
</html>
