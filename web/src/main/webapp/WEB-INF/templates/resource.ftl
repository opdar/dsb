
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"
      name="viewport">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
<meta content="no-cache" http-equiv="pragma">
<meta content="0" http-equiv="expires">
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
<script src="/js/vue.min.js" type="text/javascript"></script>
<script src="/js/common.js" type="text/javascript"></script>
<script src="http://apps.bdimg.com/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
<script src="//cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/js/material.min.js"></script>
<script src="/js/ripples.min.js"></script>

<link href="/css/common.css" rel="stylesheet" type="text/css"/>
<link href="/css/font-awesome.min.css" rel="stylesheet"/>
<link href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap Material Design -->
<link rel="stylesheet" type="text/css" href="/css/bootstrap-material-design.min.css">
<link rel="stylesheet" type="text/css" href="/css/ripples.min.css">