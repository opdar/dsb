
<div class="row" style="margin: 0 0 20px 0;background-color: #023a34;color: white;border-radius: 4px;padding: 8px 0;font-size: 16px;">
    <div class="col-sm-12" style="padding: 0">
        <div >
            <a data-toggle="collapse" href="#datasource-menu" style="text-decoration: none;"><div class="menu-title">数据源设置</div></a>
            <div style="font-size: 14px" id="datasource-menu" class="panel-collapse collapse in" >
                <div class="menu selected" >状态查询</div>
                <div class="menu " >新增数据源</div>
            </div>
            <a data-toggle="collapse" href="#status-menu" style="text-decoration: none;"><div class="menu-title">运行状态</div></a>
            <div style="font-size: 14px" id="status-menu" class="panel-collapse collapse in" >
                <div class="menu" >连接管理</div>
            </div>
        </div>
    </div>
</div>