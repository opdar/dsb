<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <#include "/resource.ftl"/>
    <title>首页</title>
</head>

<#include "/dashboard/bodytop.ftl"/>
<div style="color: #ffffff;text-align: center;">
<div class="row table-header" style="border-radius: 4px;">
    <div class="col-xs-2">数据源</div>
    <div class="col-xs-2">地址</div>
    <div class="col-xs-2">状态</div>
    <div class="col-xs-2">负载率</div>
    <div class="col-xs-2">智能负载</div>
    <div class="col-xs-2">权重</div>
</div>
    <div>
        <a href="javascript:void(0);" class="btn table-tr" >
            <div class="row ">
                <div>
                    <div class="col-xs-2">机器A</div>
                    <div class="col-xs-2">192.168.1.1</div>
                    <div class="col-xs-2">警告</div>
                    <div class="col-xs-2">98%</div>
                    <div class="col-xs-2">开启</div>
                    <div class="col-xs-2">1</div>
                </div>
            </div>
        </a>
        <a href="javascript:void(0);" class="btn table-tr" >
            <div class="row ">
                <div>
                    <div class="col-xs-2">机器B</div>
                    <div class="col-xs-2">192.168.1.2</div>
                    <div class="col-xs-2">运行中</div>
                    <div class="col-xs-2">10%</div>
                    <div class="col-xs-2">开启</div>
                    <div class="col-xs-2">5</div>
                </div>
            </div>
        </a>
    </div>
</div>
<#include "/dashboard/bodyfooter.ftl"/>
