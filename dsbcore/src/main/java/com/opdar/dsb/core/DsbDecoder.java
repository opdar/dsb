package com.opdar.dsb.core;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.DecoderException;
import io.netty.util.ReferenceCountUtil;

import java.net.SocketAddress;

/**
 * Created by 俊帆 on 2015/8/27.
 */
public class DsbDecoder extends ChannelInboundHandlerAdapter {

    public DsbDecoder() {
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        try {
            ByteBuf cast = null;
            SocketAddress address = null;
            if (msg instanceof DatagramPacket) {
                cast = ((DatagramPacket) msg).copy().content();
                address = ((DatagramPacket) msg).sender();
            } else {
                cast = (ByteBuf) msg;
                address = ctx.channel().remoteAddress();
            }
            try {
                decode(ctx, cast, address);
            } finally {
                ReferenceCountUtil.release(cast);
            }
        } catch (DecoderException e) {
            throw e;
        } catch (Exception e) {
            throw new DecoderException(e);
        }
    }

    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, SocketAddress address) throws Exception {
        ByteBuf b = byteBuf.duplicate();

        //transfer message
        ReferenceCountUtil.retain(byteBuf);
        channelHandlerContext.fireChannelRead(byteBuf);
    }

}
