package com.opdar.dsb.core;

import io.netty.channel.*;

/**
 * Created by 俊帆 on 2016/11/14.
 */
public class DsHandler extends ChannelInboundHandlerAdapter {

    private final DsbSession session;

    public DsHandler(DsbSession session) {
        this.session = session;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        ctx.read();
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, Object msg) {
        session.writeAndFlush(msg).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) {
                if (future.isSuccess()) {
                    ctx.channel().read();
                } else {
                    future.channel().close();
                }
            }
        });
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        session.stopOutbound(ctx.channel());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        session.stopOutbound(ctx.channel());
    }
}