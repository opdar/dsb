package com.opdar.dsb.core;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;

import java.util.LinkedList;

/**
 * Created by 俊帆 on 2016/11/14.
 */
public class DsbSession {

    private LinkedList<Channel> outs = new LinkedList<Channel>();
    private ChannelHandlerContext ctx;

    public DsbSession(ChannelHandlerContext ctx) {
        this.ctx = ctx;
    }

    public void create(String host, int port){
        final Channel inboundChannel = ctx.channel();
        Bootstrap b = new Bootstrap();
        b.group(inboundChannel.eventLoop())
                .channel(ctx.channel().getClass())
                .handler(new DsHandler(this))
                .option(ChannelOption.AUTO_READ, false);
        ChannelFuture f = b.connect(host, port);
        Channel outboundChannel = f.channel();
        outs.add(outboundChannel);
        f.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) {
                if (future.isSuccess()) {
                    inboundChannel.read();
                } else {
                    inboundChannel.close();
                }
            }
        });
    }

    public void stopOutbound(Channel ch){
        outs.remove(ch);
    }

    public void closeAndFlush() {
        for(Channel ch:outs){
            if (ch.isActive()) {
                ch.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
            }
        }
    }

    public Channel getDS() {
        return outs.getFirst();
    }

    public void transfer(Object msg){
        Channel ch = getDS();
        if (ch.isActive()) {
            ch.writeAndFlush(msg).addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) {
                    if (future.isSuccess()) {
                        ctx.channel().read();
                    } else {
                        future.channel().close();
                    }
                }
            });
        }
    }

    public ChannelFuture writeAndFlush(Object msg) {
        return ctx.writeAndFlush(msg);
    }
}