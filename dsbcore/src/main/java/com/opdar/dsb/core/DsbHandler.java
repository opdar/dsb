package com.opdar.dsb.core;

import io.netty.channel.*;
import io.netty.util.AttributeKey;

/**
 * Created by 俊帆 on 2016/11/14.
 */
public class DsbHandler extends ChannelInboundHandlerAdapter {

    private final AttributeKey<DsbSession> sessions = AttributeKey.valueOf("sessions");

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        DsbSession session;
        ctx.attr(sessions).set(session = new DsbSession(ctx));
        session.create("127.0.0.1",3306);
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        super.handlerAdded(ctx);
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        super.handlerRemoved(ctx);
        DsbSession session = ctx.attr(sessions).getAndRemove();
        session.closeAndFlush();
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        super.channelUnregistered(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
        cause.printStackTrace();
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, Object msg) {
        DsbSession session = ctx.attr(sessions).get();
        session.transfer(msg);
    }

}