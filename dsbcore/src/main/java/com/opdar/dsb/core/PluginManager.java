package com.opdar.dsb.core;

import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by 俊帆 on 2016/11/10.
 */
public class PluginManager extends URLClassLoader{
    public PluginManager() {
        super(new URL[]{},Thread.currentThread().getContextClassLoader());
    }
}
