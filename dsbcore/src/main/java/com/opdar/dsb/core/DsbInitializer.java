package com.opdar.dsb.core;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * Created by 俊帆 on 2016/11/14.
 */
public class DsbInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast("logger", new LoggingHandler(LogLevel.INFO));
        pipeline.addLast("decoder", new DsbDecoder());
        pipeline.addLast("handler", new DsbHandler());
    }
}